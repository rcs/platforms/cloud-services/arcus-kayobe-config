---
# Add extra Kolla global configuration here.

# Although Kolla-Ansible provides full support for Fernet tokens, it still
# defaults to UUID.  This setting overrides K-A and brings it in-line with
# Keystone's default.
#
keystone_token_provider: 'fernet'

# OIDC federated login
keystone_enable_federation_openid: "yes"

# Utilize memcached backend for OIDC caching 
keystone_oidc_enable_memcached: "yes"

# To work around issue of trying to install docker from
# empty pulp server, use upstream docker dnf repo on
# non-overcloud hosts
enable_docker_repo: "{% raw %}{{ 'overcloud' not in group_names }}{% endraw %}"

# Tell kolla to use the extra volume in this location
nova_instance_datadir_volume: '/var/lib/nova'

# Ceph integration, without nova having rbd backend
cinder_backend_ceph: "yes"
cinder_backend_ceph_name: ceph01-rbd

# Use etcd as Cinder coordinator
cinder_coordination_backend: "etcd"

# Make glance use a Ceph RBD backend
glance_backend_swift: "no"
glance_backend_file: "no"
glance_backend_ceph: "yes"

# Enable CADF notifications in Keystone
keystone_default_notifications_topic_enabled: yes

# add route to ceph-rgw group in haproxy
enable_ceph_rgw_external: "no"

# opt into new docker packages
docker_legacy_packages: False

# Using gitlab registry with TLS
docker_registry_insecure: false

# add ceph exporter:
enable_prometheus_ceph_mgr_exporter: "no"

# Kayobe currently configures chrony on each node in the overcloud
enable_chrony: false
enable_host_ntp: false

# Default size causes ES to fall over very easily
{% if scientific_openstack_trait_all_in_one %}
es_heap_size: 2g
opensearch_heap_size: "2g"
{% else %}
es_heap_size: "24g"
opensearch_heap_size: "24g"
{% endif %}
# Look after elasticserach indexes, as required
enable_elasticsearch_curator: true
# Close index after 30 days
elasticsearch_curator_soft_retention_period_days: 30
# Hard delete index after around 2 months
elasticsearch_curator_hard_retention_period_days: 60

# NOTE: We are increasing the HAProxy timeouts to 5 minutes due to the neutron
# generic switch driver, which synchronously applies switch configuration for
# each ironic port during node provisioning and tear down.

# Due to the lack of batching of configuration sets, and the long time required for configuration
# to be committed, concurrent deployment of multiple nodes can cause configuration transactions
# to back up.  The specific API calls that require this long timeout are:
# - Creation and deletion of VLAN networks.
# - Creation or update of ports, adding binding information.
# - Update of ports, removing binding information.
# - Deletion of ports.
haproxy_client_timeout: 10m30
haproxy_server_timeout: 10m30

# Increase from 10 seconds due to issues
# with eventlet taking a while to complete a request
# Note: mariadb connect timeout is also set to 30 seconds
haproxy_connect_timeout: 60s

# RMQ HA mode (durable queues)
om_enable_rabbitmq_high_availability: false

# Check keystone and horizon are up, and cert is valid
prometheus_blackbox_exporter_endpoints:
  - "{% raw %}horizon:http_2xx:{{ public_protocol }}://{{ kolla_external_fqdn | put_address_in_context('url') }}{% endraw %}"
  - "{% raw %}keystone:os_endpoint:{{ public_protocol }}://{{ kolla_external_fqdn | put_address_in_context('url') }}:{{ keystone_public_port}}{% endraw %}"
  - "{% raw %}cinder:os_endpoint:{{ public_protocol }}://{{ kolla_external_fqdn | put_address_in_context('url') }}:{{ cinder_api_port}}{% endraw %}"
  - "{% raw %}glance:os_endpoint:{{ public_protocol }}://{{ kolla_external_fqdn | put_address_in_context('url') }}:{{ glance_api_port}}{% endraw %}"
  - "{% raw %}ironic:os_endpoint:{{ public_protocol }}://{{ kolla_external_fqdn | put_address_in_context('url') }}:{{ ironic_api_port}}{% endraw %}"
  - "{% raw %}neutron:os_endpoint:{{ public_protocol }}://{{ kolla_external_fqdn | put_address_in_context('url') }}:{{ neutron_server_port}}{% endraw %}"
  - "{% raw %}nova:os_endpoint:{{ public_protocol }}://{{ kolla_external_fqdn | put_address_in_context('url') }}:{{ nova_api_port}}{% endraw %}"
  - "{% raw %}placement:os_endpoint:{{ public_protocol }}://{{ kolla_external_fqdn | put_address_in_context('url') }}:{{ placement_api_port}}{% endraw %}"
  - "gateway:icmp:{{ public_net_gateway }}"
  - "{% raw %}vip:icmp:{{ kolla_external_fqdn }}{% endraw %}"
  - "dns:icmp:8.8.8.8"

# Backport of: https://review.opendev.org/c/openstack/kolla-ansible/+/824994
# Upstream version isbackported to Victoria.
rabbitmq_remove_ha_all_policy: true

# Needed to launch VM instances in aio environment without nested virt
nova_compute_virt_type: "{{ 'qemu' if scientific_openstack_trait_all_in_one else 'kvm' }}"

# Set Ceph manager exporter endpoints
{% if groups['mgrs'] | length > 0 %}
prometheus_ceph_mgr_exporter_endpoints:
{% for host in groups['mgrs'] %}
  - "{{ admin_oc_net_name | net_ip(host) }}:9283"
{% endfor %}
{% endif %}

#############################################################################
# Reduce the kolla-ansible token expiry default from one day to one hour
fernet_token_expiry: 3600

{% if scientific_openstack_trait_all_in_one %}
keepalived_vip_prechecks: false
{% endif %}

{% if scientific_openstack_trait_ovn %}
# No kayobe support for OVN until Wallaby
enable_ovn: true
neutron_plugin_agent: 'ovn'
{% endif %}

# Allow 5 minutes for neutron l3 failover after deleting all namespaces
# before restarting the next l3 agent.
neutron_l3_agent_failover_delay: 300

# libvirt SASL enabled
libvirt_enable_sasl: true

# On EL8 use kayobe configured docker repo
enable_docker_repo: false

# Enable SRIOV agent workaround
enable_neutron_sriov: "{% raw %}{{ _enable_neutron_sriov | default(false) }}{% endraw %}"

# Disable kolla managed NTP clients
enable_chrony: false
enable_host_ntp: false

#############################################################################
# Kolla configuration

# This is templated on localhost, so use raw tags.
enable_nova_libvirt_container: false

enable_cluster_user_trust: true

# Use a dedicated service project for Octavia load-balancers
octavia_service_auth_project: octavia-lbs

# kolla_base_distro must be set here to be resolvable on a per-host basis
# This is necessary for os migrations where mixed clouds might be deployed
kolla_base_distro: "{% raw %}{{ ansible_facts.distribution | lower }}{% endraw %}"

# Use facts so this is determined correctly when the control host OS differs
# from os_distribuition.
kolla_base_distro_version: "{% raw %}{{ kolla_base_distro_version_default_map[kolla_base_distro] }}{% endraw %}"

# Convenience variable for base distro and version string.
kolla_base_distro_and_version: "{% raw %}{{ kolla_base_distro }}-{{ kolla_base_distro_version }}{% endraw %}"

# Dict of Kolla image tags to deploy for each service.
# Each key is the tag variable prefix name, and the value is another dict,
# where the key is the OS distro and the value is the tag to deploy.
# NOTE: This is defined in etc/kayobe/kolla-image-tags.yml.
kolla_image_tags:
{{ kolla_image_tags | to_nice_yaml | indent(width=4, first=true) }}

# Variables defining which tag to use for each container's image.
{{ lookup('pipe', 'python3 ' ~ kayobe_config_path ~ '/../../tools/kolla-images.py list-tag-vars') }}

# NOTE(priteau): Nova container images can use Rocky Linux 9.3 to avoid
# Libvirt/QEMU version bump.
{% if stackhpc_pulp_repo_rocky_9_minor_version < 4 %}
nova_tag: 2023.1-rocky-9-20240725T200915
{% endif %}

#############################################################################
# RabbitMQ

#############################################################################
# Monitoring and alerting related settings

# Increase the default prometheus retention period
prometheus_cmdline_extras: '--storage.tsdb.retention.time=90d --storage.tsdb.retention.size=50GB'

# Additional command line flags for node exporter to enable texfile collector for disk metrics and create textfile docker volume
prometheus_node_exporter_extra_volumes:
  - "textfile:/var/lib/node_exporter/textfile_collector"
prometheus_node_exporter_cmdline_extras: "--collector.textfile.directory=/var/lib/node_exporter/textfile_collector"

# Custom exporters
enable_prometheus_libvirt_exporter: True
enable_prometheus_rabbitmq_exporter: True

# Increase Prom OpenStack exporter timeouts for larger clouds
prometheus_openstack_exporter_interval: 10m
prometheus_openstack_exporter_timeout: 5m

# Default size causes ES/OpenSearch to fall over very easily
{% if scientific_openstack_trait_all_in_one %}
es_heap_size: 2g
opensearch_heap_size: 2g
{% else %}
es_heap_size: "24g"
opensearch_heap_size: "24g"
{% endif %}

# Use inventory hostnames as labels
prometheus_instance_label: "{% raw %}{{ ansible_facts.hostname }}{% endraw %}"

# Make openstack-exporter use Nova API version 2.1 to keep metrics the same as
# in Yoga. This is required to include a valid value for the flavor_id label on
# openstack_nova_server_status metrics.
prometheus_openstack_exporter_compute_api_version: "2.1"

# The Pulp URL must be templated by Kayobe rather than Kolla Ansible.
# The rest of the Prometheus Blackbox exporter configuration can be found in
# the Kolla inventory.
prometheus_blackbox_exporter_endpoints_kayobe:
  - endpoints:
      - "pulp:http_2xx:{{ pulp_url }}/pulp/api/v3/status/"
    enabled: "{{ seed_pulp_container_enabled | bool }}"
