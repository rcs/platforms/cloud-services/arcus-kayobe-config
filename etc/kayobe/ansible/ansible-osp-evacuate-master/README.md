# ansible-osp-evacuate

Playbooks to live-migrate evacuate an OpenStack compute host, apply patching, reboot and wait for the host to return. 

The playbook also runs a canary instance and a Spectre-Meltdown CVE checker as last steps before returning the node to service.

The playbook can be run from the OpenStack director currently, as it can SSH to computes as heat-admin and elevate to root.

We can look at making this a Rundeck task to asynchronously evacuate, patch and reboot compute hosts when needed.

## Usage

OpenStack admin credentials can be stored in a vault.yml Ansible Vault file (not stored in this repo), e.g.

	openrc_vars:
	  OS_NO_CACHE: True
	  COMPUTE_API_VERSION: 1.1
	  OS_USERNAME: admin
	  no_proxy: ",vss.cloud.private.cam.ac.uk,vss.cloud.private.cam.ac.uk"
	  OS_TENANT_NAME: admin
	  OS_CLOUDNAME: overcloud"
	  OS_AUTH_URL: "https://vss.cloud.private.cam.ac.uk:5000/v3"
	  NOVA_VERSION: "1.1"
	  OS_PASSWORD: "REDACTED"
	  OS_IDENTITY_API_VERSION: "3"
	  OS_USER_DOMAIN_NAME: "Default"
	  OS_PROJECT_DOMAIN_NAME: "Default"

Wrapper makefile for the playbook can be run with:

	make test
