#!/bin/bash

set -euf -o pipefail

function usage {
   echo "$0 <hypervisor_hostname>"
   exit 0
}

function reconfigure () {
   HOST=$1
   ANSIBLE_PB_DIR="/stack/kayobe-automation-env/src/kayobe-config/etc/kayobe/ansible"

   echo $HOST
   
   echo "# Clearing fact cache for $HOST"
   [[ -f /tmp/kayobe-facts/$HOST ]] && sudo rm -f /tmp/kayobe-facts/$HOST
   [[ -f /tmp/kolla-ansible-facts/$HOST ]] && sudo rm -f /tmp/kolla-ansible-facts/$HOST

   echo "# Reconfigure the BM hypervisor node"
   kayobe playbook run -l $HOST ${ANSIBLE_PB_DIR}/overcloud-update-trust-store.yml
   kayobe playbook run -l $HOST ${ANSIBLE_PB_DIR}/compute-lvm.yml
   kayobe overcloud host configure -l $HOST -kl $HOST
   kayobe overcloud service deploy -l $HOST -kl $HOST

   exit 0
}

[ "$#" -ne 1 ] && ( usage && exit 1 ) || reconfigure $1
