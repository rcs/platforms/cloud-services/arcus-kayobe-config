#!/bin/bash

set -euf -o pipefail

HOST=$1
PROJECT=$2

HOST_UUID=$(openstack server list -f value --project $PROJECT --name "^${HOST}$" -c ID)
BM_NODE=$(openstack server show -f value -c "OS-EXT-SRV-ATTR:hypervisor_hostname" $HOST_UUID)
BM_NODE_PXE_MAC=$(openstack baremetal node show -f json $BM_NODE  | jq -r .extra.pxe_interface_mac)
openstack baremetal port show -f yaml -c local_link_connection --address $BM_NODE_PXE_MAC
