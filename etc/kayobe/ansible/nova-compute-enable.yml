---
- name: Enable nova compute service
  hosts: compute
  gather_facts: yes
  tags:
    - nova-compute-enable
  vars:
    controller_host: "{{ groups['controllers'][0] }}"
    venv: "{{ virtualenv_path }}/openstack"
    disabled_reason: "Down for maintenance by nova-compute-disable.yml"
  tasks:
    - name: Initiate openstack cli virtualenv
      pip:
        virtualenv: "{{ venv }}"
        name:
          - pip
          - setuptools
        state: latest
        virtualenv_command: /usr/bin/python3.9 -m venv
      run_once: true
      delegate_to: "{{ controller_host }}"
      vars:
        # NOTE: Without this, the controller's ansible_host variable will not
        # be respected when using delegate_to.
        ansible_host: "{{ hostvars[controller_host].ansible_host | default(controller_host) }}"

    - name: Install openstack CLI tools in virtualenv
      pip:
        virtualenv: "{{ venv }}"
        virtualenv_command: "/usr/bin/python3 -m venv"
        name:
          - python-openstackclient
      run_once: true
      delegate_to: "{{ controller_host }}"
      vars:
        # NOTE: Without this, the controller's ansible_host variable will not
        # be respected when using delegate_to.
        ansible_host: "{{ hostvars[controller_host].ansible_host | default(controller_host) }}"

    - name: Query nova compute services
      command: >
        {{ venv }}/bin/openstack
        compute service list
        --format json
        --service nova-compute --host {{ ansible_facts.nodename }} --long
      environment: "{{ openstack_auth_env }}"
      delegate_to: "{{ groups['controllers'][0] }}"
      register: compute_services
      vars:
        ansible_host: "{{ hostvars[groups['controllers'][0]].ansible_host }}"

    - name: Enable nova compute service
      command: >
        {{ venv }}/bin/openstack
        compute service set
        {{ ansible_facts.nodename }} nova-compute
        --enable
      environment: "{{ openstack_auth_env }}"
      delegate_to: "{{ groups['controllers'][0] }}"
      vars:
        service: "{{ compute_services.stdout | from_json | first }}"
        ansible_host: "{{ hostvars[groups['controllers'][0]].ansible_host }}"
      when:
        # Don't enable the compute service if it was not disabled by
        # nova-compute-disable.yml
        - service.Status == 'disabled'
        - service['Disabled Reason'] == disabled_reason
