#!/bin/bash

set -euf -o pipefail

kayobe overcloud host configure -l clc-mon-ar06-u27 -kl clc-mon-ar06-u27
kayobe overcloud service deploy -kt prometheus -l clc-mon-ar06-u27 -kl clc-mon-ar06-u27
