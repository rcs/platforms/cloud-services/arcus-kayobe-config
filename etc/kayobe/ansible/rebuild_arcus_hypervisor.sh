#!/bin/bash

set -euf -o pipefail

function usage {
   echo "$0 <hypervisor_hostname> <arcus_project_name>"
   exit 0
}

function rebuild () {
   HOST=$1
   PROJECT=$2
   ANSIBLE_PB_DIR="/stack/kayobe-automation-env/src/kayobe-config/etc/kayobe/ansible"

   #source ~/go-train-sidegrade.sh
   echo $HOST $PROJECT
   
   echo "# Disble and drain the Nova compute host"
   kayobe playbook run ${ANSIBLE_PB_DIR}/nova-compute-disable.yml -l $HOST
   kayobe playbook run ${ANSIBLE_PB_DIR}/nova-compute-drain.yml -l $HOST -e hypervisor_project=$PROJECT
   
   #echo "# On XCAT1, run: sudo ssh-copy-id -i /root/.ssh/kayobe-hypers cloud-user@${HOST}"
   #echo "# Sleep for 60 seconds to do this"
   #sleep 60

   exit 0
}

[ "$#" -ne 2 ] && ( usage && exit 1 ) || rebuild $1 $2
