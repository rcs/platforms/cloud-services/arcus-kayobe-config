#!/bin/bash

set -e

tag=${1:-rocky}
images="centos-binary-manila-share
centos-binary-manila-scheduler
centos-binary-manila-data
centos-binary-manila-api"
registry=10.41.150.250:4000/arcus-cambridge

for image in $images; do
    sudo docker pull kolla/$image:$tag
    sudo docker tag kolla/$image:$tag $registry/$image:$tag
    sudo docker push $registry/$image:$tag
done
