#!/bin/bash

set -euf -o pipefail

BM_NODE=$1

BM_NODE_PXE_MAC=$(openstack baremetal node show -f json $BM_NODE  | jq -r .extra.pxe_interface_mac)
openstack baremetal port show -f yaml -c local_link_connection --address $BM_NODE_PXE_MAC
