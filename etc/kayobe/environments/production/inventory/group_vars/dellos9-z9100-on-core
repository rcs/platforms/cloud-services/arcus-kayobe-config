---
# High-speed Ethernet (HS) switch configuration for Dell OS9 Z9100-ON switches

# User to access the switch via SSH.
ansible_user: "{{ secrets_switch_ssh_username }}"

# Password to access the switch via SSH.
ansible_ssh_pass: "{{ secrets_switch_ssh_password }}"

# Password to enter privileged mode.
switch_auth_pass: "{{ secrets_switch_enable_password }}"

###############################################################################
# Global configuration.

# Global configuration. List of global configuration lines.
switch_config_all:
  # These first four are required, along with the management interface
  # configuration, in order to access the switch via SSH.
  - "aaa authentication enable default none"
  - "ip ssh server enable"
  - "hostname {{ inventory_hostname }}"
  - "username admin password 7 {{ secrets_switch_admin_hash }} privilege 15"
  # Disable Bare Metal Provisioning (BMP) after the first boot.
  - "reload-type"
  - " boot-type normal-reload"
  - " exit"
  # SSH server configuration.
  - "ip ssh connection-rate-limit 60"
  # LLDP configuration.
  - "protocol lldp"
  - " advertise dot3-tlv max-frame-size"
  - " advertise management-tlv management-address system-description system-name"
  - " advertise interface-port-desc description"
  - " no disable"
  # RSTP configuration
  - "protocol spanning-tree rstp"
  - " no disable"
  - " bridge-priority 24576"
  # Logging
  - "logging 10.45.255.33"
  # SNMP configuration
  - "snmp-server community {{ secrets_switch_snmp_rw_community }} rw"
  - "snmp-server community public ro"
  - "snmp-server contact sysadmin@hpc.cam.ac.uk"
  - "snmp-server enable traps snmp authentication coldstart linkdown linkup syslog-reachable syslog-unreachable"
  - "snmp-server enable traps lacp"
  - "snmp-server enable traps entity"
  - "snmp-server enable traps stp"
  - "snmp-server enable traps vlt"
  - "snmp-server enable traps xstp"
  - "snmp-server enable traps config"
  - "snmp-server enable traps envmon cam-utilization fan supply temperature"
  - "snmp-server host 10.45.101.1 traps version 2c librenms udp-port 162"
  - "snmp-server location WCDC-DH1"
  # NTP configuration
  - "ntp server 10.45.255.49"

# Interface configuration for LAG interfaces.
switch_interface_config_lag:
  - "no shutdown"
#  - "switchport"
  - "mtu 9216"

# Interface configuration for interfaces that belong to a LAG.
switch_interface_config_lag_member_on:
  - "no shutdown"
  - "no switchport"
  - "mtu 9216"
  # LLDP configuration.
  - "protocol lldp"
  - " advertise dot3-tlv max-frame-size"
  - " advertise management-tlv management-address system-description system-name"
  - " advertise interface-port-desc description"
  - " no disable"
  - " exit"


# Interface configuration for interfaces that belong to a LAG.
switch_interface_config_lag_member_off:
  - "shutdown"
  - "no switchport"
  - "mtu 9216"
  # LLDP configuration.
  - "protocol lldp"
  - " advertise dot3-tlv max-frame-size"
  - " advertise management-tlv management-address system-description system-name"
  - " advertise interface-port-desc description"
  - " no disable"
  - " exit"
