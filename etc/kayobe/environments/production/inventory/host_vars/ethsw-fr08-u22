---
# Switch configuration for ethsw-fr08-u22

# This switch is a 1G Dell S3048-ON, and provides access to both BMCs and 1GbE node PXE NICs

###############################################################################
# Switch credentials.

# Host/IP on which to access the switch via SSH.
ansible_host: 10.45.253.164

# The type of switch used for the control and provisioning network.
switch_type: dellos9

###############################################################################
# Global configuration.

# Global configuration. List of global configuration lines.
switch_config: "{{ switch_config_all }}"

# Interface configuration. Dict mapping switch interface names to configuration
# dicts. Each dict contains a description item and a 'config' item which should
# contain a list of per-interface configuration.
switch_interface_config:
#####
# Host access ports, by chassis
# U37-U38
  GigabitEthernet1/1:
    description: svn4-fr08-u37
    config: "{{ switch_interface_config_bm_node }}"
  GigabitEthernet1/2:
    description: svn3-fr08-u38
    config: "{{ switch_interface_config_bm_node }}"
  GigabitEthernet1/3:
    description: svn2-fr08-u37
    config: "{{ switch_interface_config_bm_node }}"
  GigabitEthernet1/4:
    description: svn1-fr08-u38
    config: "{{ switch_interface_config_bm_node }}"
# U35-U36
  GigabitEthernet1/5:
    description: svn4-fr08-u35
    config: "{{ switch_interface_config_bm_node }}"
  GigabitEthernet1/6:
    description: svn3-fr08-u36
    config: "{{ switch_interface_config_bm_node }}"
  GigabitEthernet1/7:
    description: svn2-fr08-u35
    config: "{{ switch_interface_config_bm_node }}"
  GigabitEthernet1/8:
    description: svn1-fr08-u36
    config: "{{ switch_interface_config_bm_node }}"
# U33-U34
  GigabitEthernet1/9:
    description: svn4-fr08-u33
    config: "{{ switch_interface_config_bm_node }}"
  GigabitEthernet1/10:
    description: svn3-fr08-u34
    config: "{{ switch_interface_config_bm_node }}"
  GigabitEthernet1/11:
    description: svn2-fr08-u33
    config: "{{ switch_interface_config_bm_node }}"
  GigabitEthernet1/12:
    description: svn1-fr08-u34
    config: "{{ switch_interface_config_bm_node }}"
# U31-U32
  GigabitEthernet1/13:
    description: svn4-fr08-u31
    config: "{{ switch_interface_config_bm_node }}"
  GigabitEthernet1/14:
    description: svn3-fr08-u32
    config: "{{ switch_interface_config_bm_node }}"
  GigabitEthernet1/15:
    description: svn2-fr08-u31
    config: "{{ switch_interface_config_bm_node }}"
  GigabitEthernet1/16:
    description: svn1-fr08-u32
    config: "{{ switch_interface_config_bm_node }}"
# U29-U30
  GigabitEthernet1/17:
    description: svn4-fr08-u29
    config: "{{ switch_interface_config_bm_node }}"
  GigabitEthernet1/18:
    description: svn3-fr08-u30
    config: "{{ switch_interface_config_bm_node }}"
  GigabitEthernet1/19:
    description: svn2-fr08-u29
    config: "{{ switch_interface_config_bm_node }}"
  GigabitEthernet1/20:
    description: svn1-fr08-u30
    config: "{{ switch_interface_config_bm_node }}"
# U27-U28
  GigabitEthernet1/21:
    description: svn4-fr08-u27
    config: "{{ switch_interface_config_bm_node }}"
  GigabitEthernet1/22:
    description: svn3-fr08-u28
    config: "{{ switch_interface_config_bm_node }}"
  GigabitEthernet1/23:
    description: svn2-fr08-u27
    config: "{{ switch_interface_config_bm_node }}"
  GigabitEthernet1/24:
    description: svn1-fr08-u28
    config: "{{ switch_interface_config_bm_node }}"
# U25-U26
  GigabitEthernet1/25:
    description: svn4-fr08-u25
    config: "{{ switch_interface_config_bm_node }}"
  GigabitEthernet1/26:
    description: svn3-fr08-u26
    config: "{{ switch_interface_config_bm_node }}"
  GigabitEthernet1/27:
    description: svn2-fr08-u25
    config: "{{ switch_interface_config_bm_node }}"
  GigabitEthernet1/28:
    description: svn1-fr08-u26
    config: "{{ switch_interface_config_bm_node }}"
#####
# Uplink port channel
  TenGigabitEthernet1/49:
    description: ethsw-s-ar09-u22
    config: >
      {{ switch_interface_config_lag_member +
         ['port-channel-protocol lacp',
          ' port-channel 49 mode active',
          ' exit'] }}
  TenGigabitEthernet1/50:
    description: ethsw-s-ar12-u22
    config: >
      {{ switch_interface_config_lag_member +
         ['port-channel-protocol lacp',
          ' port-channel 49 mode active',
          ' exit'] }}
  Po49:
    description: ethsw-s-u22-uplink
    config: "{{ switch_interface_config_lag }}"
# VLAN interfaces
  vlan 43:
    description: User-Data-43
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 49"
  vlan 45:
    description: Admin-IPMI-45
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 49"
      # Untagged on DRAC access ports
      - "tagged gigabitethernet 1/1-1/28"
  vlan 610:
    description: arcus-Prod-610-WInspect
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 49"
  vlan 611:
    description: arcus-Prod-611-WProv
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 49"
  vlan 613:
    description: arcus-Prod-613-WClean
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 49"

# Interface configuration for enabling hardware discovery. After discovery
# Neutron owns the configuration of these ports. Has the same format as
# switch_interface_config.
switch_interface_config_enable_discovery:
  vlan 610:
    description: arcus-Prod-610-WInspect
    # Compute nodes.
    config: >
      {{ switch_interface_config['vlan 610'].config +
         ['untagged gigabitethernet 1/1-1/28'] }}

# Interface configuration for disabling hardware discovery. After discovery
# Neutron owns the configuration of these ports. Has the same format as
# switch_interface_config.
switch_interface_config_disable_discovery:
  vlan 610:
    description: arcus-Prod-610-WInspect
    # Compute nodes.
    config: >
      {{ switch_interface_config['vlan 610'].config +
         ['no untagged gigabitethernet 1/1-1/28'] }}
