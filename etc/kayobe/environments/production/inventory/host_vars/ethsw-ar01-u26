---
# Switch configuration for ethsw-ar01-u26

# This switch is a 1G Dell S3048-ON, and provides dedicated node BMC management
# links and node in-band data-plane links

###############################################################################
# Switch credentials.

# Host/IP on which to access the switch via SSH.
ansible_host: 10.45.253.125

# The type of switch used for the control and provisioning network.
switch_type: dellos9

###############################################################################
# Global configuration.

# Global configuration. List of global configuration lines.
switch_config: "{{ switch_config_all }}"

# Interface configuration. Dict mapping switch interface names to configuration
# dicts. Each dict contains a description item and a 'config' item which should
# contain a list of per-interface configuration.
switch_interface_config:
#####
# Host access ports, by chassis
  GigabitEthernet1/1:
    description: sv-ar01-u1
    config: "{{ switch_interface_config_bm_node_on }}"
  GigabitEthernet1/2:
    description: sv-ar01-u3
    config: "{{ switch_interface_config_bm_node_on }}"
  GigabitEthernet1/3:
    description: sv-ar01-u5
    config: "{{ switch_interface_config_bm_node_on }}"
  GigabitEthernet1/4:
    description: sv-ar01-u7
    config: "{{ switch_interface_config_bm_node_on }}"
  GigabitEthernet1/5:
    description: sv-ar01-u9
    config: "{{ switch_interface_config_bm_node_on }}"
  GigabitEthernet1/6:
    description: sv-ar01-u11
    config: "{{ switch_interface_config_bm_node_on }}"

#####
# Uplink port channel
  TenGigabitEthernet1/51:
    description: ethsw-s-ar09-u24-p22-1
    config: >
      {{ switch_interface_config_lag_member +
         ['port-channel-protocol lacp',
          ' port-channel 51 mode active',
          ' exit'] }}
  TenGigabitEthernet1/52:
    description: ethsw-s-ar12-u24-p22-1
    config: >
      {{ switch_interface_config_lag_member +
         ['port-channel-protocol lacp',
          ' port-channel 51 mode active',
          ' exit'] }}
  Po51:
    description: ethsw-s-u24-uplink
    config: "{{ switch_interface_config_lag }}"
# VLAN interfaces
  vlan 41:
    description: Sysadmin-MGT-41
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 51"
  vlan 45:
    description: Admin-IPMI-45
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 51"
      # Untagged on dedicated DRAC access ports
      #- "untagged gigabitethernet 1/1-1/6"
  vlan 610:
    description: arcus-Prod-610-WInspect
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 51"
  vlan 611:
    description: arcus-Prod-611-WProv
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 51"
  vlan 613:
    description: arcus-Prod-613-WClean
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 51"

# Interface configuration for enabling hardware discovery. After discovery
# Neutron owns the configuration of these ports. Has the same format as
# switch_interface_config.
switch_interface_config_enable_discovery:
  vlan 610:
    description: arcus-Prod-610-WInspect
    # Compute nodes.
    config: >
      {{ switch_interface_config['vlan 610'].config +
         ['untagged gigabitethernet 1/1-1/6'] }}

# Interface configuration for disabling hardware discovery. After discovery
# Neutron owns the configuration of these ports. Has the same format as
# switch_interface_config.
switch_interface_config_disable_discovery:
  vlan 610:
    description: arcus-Prod-610-WInspect
    # Compute nodes.
    config: >
      {{ switch_interface_config['vlan 610'].config +
         ['no untagged gigabitethernet 1/1-1/6'] }}
