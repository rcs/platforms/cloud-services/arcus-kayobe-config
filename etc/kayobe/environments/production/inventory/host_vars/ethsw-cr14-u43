---
# Switch configuration for ethsw-cr14-u43

# This switch is a 1G Dell S3048-ON, and provides dedicated node BMC management
# links, DPU management links and node in-band data-plane links

###############################################################################
# Switch credentials.

# Host/IP on which to access the switch via SSH.
ansible_host: 10.45.253.36

# The type of switch used for the control and provisioning network.
switch_type: dellos9

###############################################################################
# Global configuration.

# Global configuration. List of global configuration lines.
switch_config: "{{ switch_config_all }}"

# Interface configuration. Dict mapping switch interface names to configuration
# dicts. Each dict contains a description item and a 'config' item which should
# contain a list of per-interface configuration.
switch_interface_config:
#####
# Host access ports, by chassis
# U39
  GigabitEthernet1/1:
    description: sv-cr14-u39-idrac
    config: "{{ switch_interface_config_bm_node_on }}"
  GigabitEthernet1/2:
    description: sv-cr14-u39-1g-p1
    config: "{{ switch_interface_config_bm_node_on }}"
  GigabitEthernet1/3:
    description: sv-cr14-u39-1g-bl
    config: "{{ switch_interface_config_bm_node_on }}"
# U35
  GigabitEthernet1/4:
    description: sv-cr14-u35-idrac
    config: "{{ switch_interface_config_bm_node_on }}"
  GigabitEthernet1/5:
    description: sv-cr14-u35-1g-p1
    config: "{{ switch_interface_config_bm_node_on }}"
  GigabitEthernet1/6:
    description: sv-cr14-u35-1g-bl
    config: "{{ switch_interface_config_bm_node_on }}"
# U31
  GigabitEthernet1/7:
    description: sv-cr14-u31-idrac
    config: "{{ switch_interface_config_bm_node_on }}"
  GigabitEthernet1/8:
    description: sv-cr14-u31-1g-p1
    config: "{{ switch_interface_config_bm_node_on }}"
  GigabitEthernet1/9:
    description: sv-cr14-u31-1g-bl
    config: "{{ switch_interface_config_bm_node_on }}"
# U27
  GigabitEthernet1/10:
    description: sv-cr14-u27-idrac
    config: "{{ switch_interface_config_bm_node_on }}"
  GigabitEthernet1/11:
    description: sv-cr14-u27-1g-p1
    config: "{{ switch_interface_config_bm_node_on }}"
  GigabitEthernet1/12:
    description: sv-cr14-u27-1g-bl
    config: "{{ switch_interface_config_bm_node_on }}"
# U23
  GigabitEthernet1/13:
    description: sv-cr14-u23-idrac
    config: "{{ switch_interface_config_bm_node_on }}"
  GigabitEthernet1/14:
    description: sv-cr14-u23-1g-p1
    config: "{{ switch_interface_config_bm_node_on }}"
  GigabitEthernet1/15:
    description: sv-cr14-u23-1g-bl
    config: "{{ switch_interface_config_bm_node_on }}"
# U17
  GigabitEthernet1/16:
    description: sv-cr14-u17-idrac
    config: "{{ switch_interface_config_bm_node_on }}"
  GigabitEthernet1/17:
    description: sv-cr14-u17-1g-p1
    config: "{{ switch_interface_config_bm_node_on }}"
  GigabitEthernet1/18:
    description: sv-cr14-u17-1g-bl
    config: "{{ switch_interface_config_bm_node_on }}"
# U13
  GigabitEthernet1/19:
    description: sv-cr14-u13-idrac
    config: "{{ switch_interface_config_bm_node_on }}"
  GigabitEthernet1/20:
    description: sv-cr14-u13-1g-p1
    config: "{{ switch_interface_config_bm_node_on }}"
  GigabitEthernet1/21:
    description: sv-cr14-u13-1g-bl
    config: "{{ switch_interface_config_bm_node_on }}"
# U9
  GigabitEthernet1/22:
    description: sv-cr14-u9-idrac
    config: "{{ switch_interface_config_bm_node_on }}"
  GigabitEthernet1/23:
    description: sv-cr14-u9-1g-p1
    config: "{{ switch_interface_config_bm_node_on }}"
  GigabitEthernet1/24:
    description: sv-cr14-u9-1g-bl
    config: "{{ switch_interface_config_bm_node_on }}"
# U5
  GigabitEthernet1/25:
    description: sv-cr14-u5-idrac
    config: "{{ switch_interface_config_bm_node_on }}"
  GigabitEthernet1/26:
    description: sv-cr14-u5-1g-p1
    config: "{{ switch_interface_config_bm_node_on }}"
  GigabitEthernet1/27:
    description: sv-cr14-u5-1g-bl
    config: "{{ switch_interface_config_bm_node_on }}"
# U1
  GigabitEthernet1/28:
    description: sv-cr14-u1-idrac
    config: "{{ switch_interface_config_bm_node_on }}"
  GigabitEthernet1/29:
    description: sv-cr14-u1-1g-p1
    config: "{{ switch_interface_config_bm_node_on }}"
  GigabitEthernet1/30:
    description: sv-cr14-u1-1g-bl
    config: "{{ switch_interface_config_bm_node_on }}"

#####
# Uplink port channel
  TenGigabitEthernet1/49:
    description: ethsw-s-ar09-u22-p13-1
    config: >
      {{ switch_interface_config_lag_member +
         ['port-channel-protocol lacp',
          ' port-channel 49 mode active',
          ' exit'] }}
  TenGigabitEthernet1/50:
    description: ethsw-s-ar12-u22-p13-1
    config: >
      {{ switch_interface_config_lag_member +
         ['port-channel-protocol lacp',
          ' port-channel 49 mode active',
          ' exit'] }}
  Po49:
    description: ethsw-s-u22-uplink
    config: "{{ switch_interface_config_lag }}"
# VLAN interfaces
  vlan 43:
    description: User-Data-43
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 49"
  vlan 45:
    description: Admin-IPMI-45
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 49"
      # Untagged on dedicated DRAC access ports
      - "untagged gigabitethernet 1/1,1/4,1/7,1/10,1/13"
      - "untagged gigabitethernet 1/16,1/19,1/22,1/25,1/28"
      # Untagged on DPU management access ports
      - "untagged gigabitethernet 1/3,1/6,1/9,1/12,1/15"
      - "untagged gigabitethernet 1/18,1/21,1/24,1/27,1/30"
  vlan 610:
    description: arcus-Prod-610-WInspect
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 49"
  vlan 611:
    description: arcus-Prod-611-WProv
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 49"
  vlan 613:
    description: arcus-Prod-613-WClean
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 49"

# Interface configuration for enabling hardware discovery. After discovery
# Neutron owns the configuration of these ports. Has the same format as
# switch_interface_config.
switch_interface_config_enable_discovery:
  vlan 610:
    description: arcus-Prod-610-WInspect
    # Compute nodes.
    config: >
      {{ switch_interface_config['vlan 610'].config +
         ['untagged gigabitethernet 1/2,1/5,1/8,1/11,1/14,1/17,1/20,1/23,1/26,1/29'] }}

# Interface configuration for disabling hardware discovery. After discovery
# Neutron owns the configuration of these ports. Has the same format as
# switch_interface_config.
switch_interface_config_disable_discovery:
  vlan 610:
    description: arcus-Prod-610-WInspect
    # Compute nodes.
    config: >
      {{ switch_interface_config['vlan 610'].config +
         ['no untagged gigabitethernet 1/2,1/5,1/8,1/11,1/14,1/17,1/20,1/23,1/26,1/29'] }}
