---
# Switch configuration for ethsw-ar15-u37

# This switch is a 40G Dell S6010-ON, and provides high-speed Ethernet access

###############################################################################
# Switch credentials.

# Host/IP on which to access the switch via SSH.
ansible_host: 10.45.253.227

# The type of switch used for the control and provisioning network.
switch_type: dellos9

###############################################################################
# Global configuration.

# Global configuration. List of global configuration lines.
switch_config: "{{ switch_config_all + switch_config_quad }}"

switch_config_quad:
  # Place required ports into quad mode.
  - "stack-unit 1 port 1 portmode quad"
  - "stack-unit 1 port 2 portmode quad"
  - "stack-unit 1 port 3 portmode quad"
  - "stack-unit 1 port 4 portmode quad"
  - "stack-unit 1 port 5 portmode quad"
  - "stack-unit 1 port 6 portmode quad"
  - "stack-unit 1 port 7 portmode quad"
  - "stack-unit 1 port 8 portmode quad"
  - "stack-unit 1 port 9 portmode quad"
  - "stack-unit 1 port 10 portmode quad"
  - "stack-unit 1 port 11 portmode quad"
  - "stack-unit 1 port 12 portmode quad"
  - "stack-unit 1 port 17 portmode quad"
  - "stack-unit 1 port 18 portmode quad"
  - "stack-unit 1 port 19 portmode quad"
  - "stack-unit 1 port 20 portmode quad"
  - "stack-unit 1 port 21 portmode quad"
  - "stack-unit 1 port 22 portmode quad"
  - "stack-unit 1 port 23 portmode quad"
  - "stack-unit 1 port 24 portmode quad"
  - "stack-unit 1 port 25 portmode quad"
  - "stack-unit 1 port 26 portmode quad"
  - "stack-unit 1 port 27 portmode quad"
  - "stack-unit 1 port 28 portmode quad"

# Interface configuration. Dict mapping switch interface names to configuration
# dicts. Each dict contains a description item and a 'config' item which should
# contain a list of per-interface configuration.
switch_interface_config:
#####
  TenGigabitEthernet1/1/1:
    description: sv-ar15-u21-10g-c1p1
    config: >
      {{ switch_interface_config_lag_member +
         ['port-channel-protocol lacp',
          ' port-channel 1 mode active',
          ' exit'] }}
  TenGigabitEthernet1/1/2:
    description: sv-ar15-u21-10g-c1p2
    config: >
      {{ switch_interface_config_lag_member +
         ['port-channel-protocol lacp',
          ' port-channel 2 mode active',
          ' exit'] }}
  TenGigabitEthernet1/1/3:
    description: sv-ar15-u35-10g-c1p1
    config: >
      {{ switch_interface_config_lag_member +
         ['port-channel-protocol lacp',
          ' port-channel 5 mode active',
          ' exit'] }}
  TenGigabitEthernet1/1/4:
    description: sv-ar15-u35-10g-c1p2
    config: >
      {{ switch_interface_config_lag_member +
         ['port-channel-protocol lacp',
          ' port-channel 6 mode active',
          ' exit'] }}
#####
  TenGigabitEthernet1/2/1:
    description: sv-ar15-u21-10g-c2p1
    config: >
      {{ switch_interface_config_lag_member +
         ['port-channel-protocol lacp',
          ' port-channel 1 mode active',
          ' exit'] }}
  TenGigabitEthernet1/2/2:
    description: sv-ar15-u21-10g-c2p2
    config: >
      {{ switch_interface_config_lag_member +
         ['port-channel-protocol lacp',
          ' port-channel 2 mode active',
          ' exit'] }}
  TenGigabitEthernet1/2/3:
    description: sv-ar15-u35-10g-c2p1
    config: >
      {{ switch_interface_config_lag_member +
         ['port-channel-protocol lacp',
          ' port-channel 5 mode active',
          ' exit'] }}
  TenGigabitEthernet1/2/4:
    description: sv-ar15-u35-10g-c2p2
    config: >
      {{ switch_interface_config_lag_member +
         ['port-channel-protocol lacp',
          ' port-channel 6 mode active',
          ' exit'] }}
#####
  TenGigabitEthernet1/3/1:
    description: sv-ar15-u23-10g-c1p1
    config: >
      {{ switch_interface_config_lag_member +
         ['port-channel-protocol lacp',
          ' port-channel 3 mode active',
          ' exit'] }}
  TenGigabitEthernet1/3/2:
    description: sv-ar15-u23-10g-c2p1
    config: >
      {{ switch_interface_config_lag_member +
         ['port-channel-protocol lacp',
          ' port-channel 3 mode active',
          ' exit'] }}
  TenGigabitEthernet1/3/3:
    description: sv-ar15-u23-10g-c1p2
    config: >
      {{ switch_interface_config_lag_member +
         ['port-channel-protocol lacp',
          ' port-channel 4 mode active',
          ' exit'] }}
  TenGigabitEthernet1/3/4:
    description: sv-ar15-u23-10g-c2p2
    config: >
      {{ switch_interface_config_lag_member +
         ['port-channel-protocol lacp',
          ' port-channel 4 mode active',
          ' exit'] }}
#####
  FortyGigE1/31:
    description: ethsw-s-ar09-u24-p23
    config: >
      {{ switch_interface_config_lag_member +
         ['port-channel-protocol lacp',
          ' port-channel 31 mode active',
          ' exit'] }}
  FortyGigE1/32:
    description: ethsw-s-ar12-u24-p23
    config: >
      {{ switch_interface_config_lag_member +
         ['port-channel-protocol lacp',
          ' port-channel 31 mode active',
          ' exit'] }}
#####
# Port channel interfaces
# NexentaStor port channels
  Po1:
    description: nstore1-external
    config: "{{ switch_interface_config_lag }}"
  Po2:
    description: nstore1-internal
    config: "{{ switch_interface_config_lag }}"
  Po3:
    description: nstore2-external
    config: "{{ switch_interface_config_lag }}"
  Po4:
    description: nstore2-internal
    config: "{{ switch_interface_config_lag }}"
# gw-wcdc-01 port channels
  Po5:
    description: gw-wcdc-01-external
    config: "{{ switch_interface_config_lag }}"
  Po6:
    description: gw-wcdc-01-internal
    config: "{{ switch_interface_config_lag }}"
# Uplink port channel
  Po31:
    description: ethsw-s-u24-uplink
    config: "{{ switch_interface_config_lag }}"
####
# VLAN interfaces
  vlan 2:
    description: Prod-OSP-Provision
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 31"
  vlan 4:
    description: Prod-OSP-Storage
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 31"
      # Tagged on host ports
      - "tagged Po 1, 2, 3, 4, 6"
  vlan 20:
    description: NSTOR-REPL
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 31"
      # Tagged on host ports
      - "tagged Po 2, 4"
  vlan 40:
    description: Sysadmin-Data-40
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 31"
      # Tagged on host ports
      - "tagged Po 1, 3, 6"
  vlan 41:
    description: Sysadmin-MGT-41
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 31"
      - "tagged Po 6"
  vlan 43:
    description: User-Data-43
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 31"
      # Tagged on host ports
      - "tagged Po 1, 3, 6"
  vlan 45:
    description: Admin-IPMI-45
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 31"
      # Tagged on host ports
      - "tagged Po 6"
  vlan 49:
    description:
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 31"
      # Tagged on host ports
      - "tagged Po 6"
  vlan 50:
    description: WBIC-Lustre-50
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 31"
      # Tagged on host ports
      - "tagged Po 1, 3, 6"
  vlan 60:
    description: ILAB-Public-60
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 31"
      # Tagged on host ports
      - "tagged Po 1, 3"
  vlan 111:
    description: CUDN-Public-111
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 31"
      # Tagged on host ports
      - "untagged Po 5"
  vlan 112:
    description: CUDN-Public-112
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 31"
      # Tagged on host ports
      - "tagged Po 1, 3"
  vlan 124:
    description: Stag-OSP-Storage-sclt100
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 31"
      # Tagged on host ports
      - "tagged Po 1, 2, 3, 4"
  vlan 134:
    description: Stag-OSP-Storage
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 31"
      # Tagged on host ports
      - "tagged Po 1, 3"
  vlan 141:
    description: Sysadmin-MGT-141
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 31"
      # Tagged on host ports
      - "tagged Po 6"
  vlan 143:
    description: User-Data-143
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 31"
      # Tagged on host ports
      - "tagged Po 1, 3, 6"
  vlan 190:
    description: SRCP-Storage-190
    config:
      - "no shutdown"
      # Tagged on uplink
      - "tagged Po 31"
      # Tagged on host ports
      - "tagged Po 1, 3"
