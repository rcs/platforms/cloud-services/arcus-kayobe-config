# This file is used to configure kayobe-automation.
# https://github.com/stackhpc/kayobe-automation/blob/main/README.md

# See: https://github.com/stackhpc/docker-rally/blob/master/bin/rally-verify-wrapper.sh for a full list of tempest parameters that can be overriden.
# You can override tempest parameters like so:

# The Tempest concurrency determines how many tests can be running at once.
# Higher values run tests faster but risk running out of resources and failing tests
# On production systems, Tempest concurrency can usually be set to a high number e.g. 16-64. It is often limited by the number of available floating IPs.
# On virtualised test environments, compute and networking speeds often limit the concurrency to 1-16 before tests begin to fail due to timeouts.
export TEMPEST_CONCURRENCY=2


# Specify single test whilst experimenting
#export TEMPEST_PATTERN="${TEMPEST_PATTERN:-tempest.api.compute.servers.test_create_server.ServersTestJSON.test_host_name_is_same_as_server_name}"

KAYOBE_AUTOMATION_TEMPEST_CONF_OVERRIDES="${KAYOBE_AUTOMATION_CONFIG_PATH}/tempest/tempest.overrides.conf"

# Tell rally not to create resources e.g flavors, networks. This allows us to
# run as non-admin.
export RALLY_CONF_ENABLE_CREATE_TEMPEST_RESOURCES=false

# Use the wallaby version of tempest
export KAYOBE_AUTOMATION_RALLY_TAG="v1.1.0"

if [ ! -z ${KAYOBE_ENVIRONMENT:+x} ]; then
  KAYOBE_AUTOMATION_TEMPEST_CONF_OVERRIDES="${KAYOBE_AUTOMATION_CONFIG_PATH}/tempest/tempest-${KAYOBE_ENVIRONMENT}-${KAYOBE_AUTOMATION_TEMPEST_LOADLIST:-}.overrides.conf"

  # Check if loadlist specific overrides exist, if not fallback to environment overrides.
  if [ ! -e "${KAYOBE_AUTOMATION_TEMPEST_CONF_OVERRIDES}" ]; then
      KAYOBE_AUTOMATION_TEMPEST_CONF_OVERRIDES="${KAYOBE_AUTOMATION_CONFIG_PATH}/tempest/tempest-${KAYOBE_ENVIRONMENT}.overrides.conf"
  fi

  log_info "Selecting overrides: $KAYOBE_AUTOMATION_TEMPEST_CONF_OVERRIDES"

  if [ "$KAYOBE_ENVIRONMENT" == "aio" ] || [ "$KAYOBE_ENVIRONMENT" == "aio-rocky" ]; then
    # Seem to get servers failing to spawn with higher concurrency
    export TEMPEST_CONCURRENCY=1
  fi

  if [[ "$KAYOBE_ENVIRONMENT" =~ "ci-multinode" ]]; then
    export TEMPEST_CONCURRENCY=4
    # Uncomment this to perform a full tempest test
    # export KAYOBE_AUTOMATION_TEMPEST_LOADLIST=tempest-full
    # export KAYOBE_AUTOMATION_TEMPEST_SKIPLIST=ci-multinode-tempest-full
  fi

  if [ "$KAYOBE_ENVIRONMENT" == "production" ]; then
     if [ ${KAYOBE_AUTOMATION_CONFIG_DIFF:-0} -eq 1 ]; then
        export KOLLA_LIMIT='clc-ar06-u19,clc-mon-ar06-u27,cl-ar06-u28,svn2-cr03-u7,cl-ar06-u29,cl-ar06-u30,cl-ar06-u42,cl-ar06-u1,cl1-cr03-u24,nova-svn4-er19-u33,nova-gpu-er20-u39,nova-sv-er19-u35'
     fi
  fi

  export KAYOBE_AUTOMATION_TEMPEST_ACCOUNTS="${KAYOBE_ENVIRONMENT}.yml"

fi

KAYOBE_AUTOMATION_CONFIG_DIFF_INJECT_FACTS=1
KAYOBE_AUTOMATION_CONFIG_DIFF_AUTO_UNSET_ENVIRONMENT=1

KAYOBE_CONFIG_SECRET_PATHS_EXTRA=(
  'etc/kayobe/kolla/inventory/group_vars/network/secrets.yml'
  "etc/kayobe/environments/$KAYOBE_ENVIRONMENT/vault-secrets.yml"
  "etc/kayobe/environments/$KAYOBE_ENVIRONMENT/arcus-idp-conf/idp/*"
)
if [[ -z "${KAYOBE_AUTOMATION_TEMPEST_CONF_OVERRIDES:+x}" ]] || [[ ! -e "${KAYOBE_AUTOMATION_TEMPEST_CONF_OVERRIDES}" ]]; then
    KAYOBE_AUTOMATION_TEMPEST_CONF_OVERRIDES="${KAYOBE_AUTOMATION_CONFIG_PATH}/tempest/tempest.overrides.conf"
fi

if [[ -f ${KAYOBE_AUTOMATION_REPO_ROOT}/etc/kolla/public-openrc.sh ]]; then
    export TEMPEST_OPENRC="$(< ${KAYOBE_AUTOMATION_REPO_ROOT}/etc/kolla/public-openrc.sh)"
fi

export ANSIBLE_VERBOSITY=0
