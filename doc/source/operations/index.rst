==============
Operator Guide
==============

This guide is for operators of the StackHPC Kayobe configuration project.

.. toctree::
   :maxdepth: 1

   ceph-management
   control-plane-operation
   customising-horizon
   gpu-in-openstack
   bifrost-hardware-inventory-management
   hotfix-playbook
   migrating-vm
   nova-compute-ironic
   octavia
   openstack-projects-and-users-management
   openstack-reconfiguration
   rabbitmq
   secret-rotation
   tempest
   upgrading-openstack
   upgrading-ceph
