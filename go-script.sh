#!/bin/bash                                      
source ~/venvs/kayobe/bin/activate
source ~/src/arcus-kayobe-config/kayobe-env
echo "Arcus Kayobe config Vault password: "
read -sr KAYOBE_VAULT_PASSWORD_INPUT
export KAYOBE_VAULT_PASSWORD=$KAYOBE_VAULT_PASSWORD_INPUT
export ANSIBLE_VAULT_PASSWORD=$KAYOBE_VAULT_PASSWORD_INPUT
cd ~/src/arcus-kayobe-config
source ~/kayobe-complete
